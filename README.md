# Project for the ATLAS Open Data software developments

**a)** This framework can be used for data analysis for experimental high energy physics.

**b)** This code is made of two parts of codes: analysis codes and plotting codes.

**c)** For analysis codes, you can check the cpp-framework folder.

**d)** For plotting codes, you can check the Plotting_HWW folder.

## To create you own analysis:

**0)** If you want to create a for example "XAnalysis", you can follow the steps below.
  
**1)** In cpp-framework folder:
  
* Create the file XAnalysis.C and change the cut selections to what you want, also change all the files name to XAnalysis.C+ in main_X.C.
* Create the files "XAnalysis.h" and "Xhistograms.h" and include this file to XAnalysis.C.
* If you want to fill your own histograms in XAnalysis.C, you can add the definition in both XAnalysis.h and Xhistograms.h files.

**2)** In Plotting_HWW folder:

* Change the input file name to what you get from the cpp-framework in Plotting_HWW.cxx.
* Make your own histograms name with HistoList_X.txt.

<hr>



