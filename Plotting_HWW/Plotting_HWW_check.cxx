/////////////////////////////////////////////////////////
//// Plotting code
//// Author: ATLAS Collaboration (2018) with implementations by: Leonid Serkin,...
/////////////////////////////////////////////////////////////

// include ROOT and C++ headers
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <math.h>
#include <cstring>
#include <TF1.h>
#include <TH1.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TEnv.h>
#include "THStack.h"
#include "TFile.h"
#include "TTree.h"
#include "TKey.h"
#include "TCanvas.h"
#include <TStyle.h>
#include "TLatex.h"
#include "TImage.h"
#include "TLine.h"
#include "TColor.h"
#include "TROOT.h"
#include "TH2F.h"
#include "TMath.h"
#include "TPaveText.h"
#include "TFrame.h"
#include "TArrow.h"
#include <TGaxis.h>

// include main header
#include "Plotting_HWW.h"

// debugging flag, set to 1 for checks
#define DEBUG 1


//////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[]){
	
	if(argc < 2){
        std::cout<<"usage: ./plot option [W,Z,top.../lumi]"<<std::endl;
        std::cout<<"output stored in a directory \'histograms\' " <<std::endl;
	//  exit(1);
	}

	Plotting_HWW* m = new Plotting_HWW();

// in case this is needed, can be given as input parameter
//	m->SetLumi(atof(argv[1]));
        m->SetLumi(1000);

	m->run();

	delete m;
	return 0;
}
///
Plotting_HWW::Plotting_HWW(){
  lumi = 0;
}

///
Plotting_HWW::~Plotting_HWW(){
}

///
void Plotting_HWW::SetLumi(double l){
  lumi = l;
}

///
void Plotting_HWW::run(){

  Disclaimer();

  getHistoSettings();

  AtlasStyle();
      
  WhichFiles();
  
  readFiles(); 
  
  makePlots();

  return;
}

///
void Plotting_HWW::Disclaimer(){

  std::cout << "\n ---------------------DISCLAIMER--------------------- \n" << std::endl ;
  std::cout << "This Software is intended for educational use only!" << std::endl ;
  std::cout << "Under no circumstances does it qualify to reproduce actual ATLAS analysis results or produce publishable results! " << std::endl ;
  std::cout << "\n ---------------------------------------------------- \n" << std::endl ;

}

///
void Plotting_HWW::AtlasStyle(){
  TStyle *atlasStyle = new TStyle("ATLAS","Atlas style");
  Int_t icol=0; // WHITE
  atlasStyle->SetFrameBorderMode(icol);
  atlasStyle->SetFrameFillColor(icol);
  atlasStyle->SetCanvasBorderMode(icol);
  atlasStyle->SetCanvasColor(icol);
  atlasStyle->SetPadBorderMode(icol);
  atlasStyle->SetPadColor(icol);
  atlasStyle->SetStatColor(icol);
  atlasStyle->SetPaperSize(20,26);
  atlasStyle->SetPadTopMargin(0.05);
  atlasStyle->SetPadRightMargin(0.05); 
  atlasStyle->SetPadBottomMargin(0.16);
  atlasStyle->SetPadLeftMargin(0.16); 
  atlasStyle->SetTitleXOffset(1.4);
  atlasStyle->SetTitleYOffset(1.4);
  Int_t font=42; // Helvetica
  Double_t tsize=0.05;
  atlasStyle->SetTextFont(font);
  atlasStyle->SetTextSize(tsize);
  atlasStyle->SetLabelFont(font,"x");
  atlasStyle->SetTitleFont(font,"x");
  atlasStyle->SetLabelFont(font,"y");
  atlasStyle->SetTitleFont(font,"y");
  atlasStyle->SetLabelFont(font,"z");
  atlasStyle->SetTitleFont(font,"z");
  atlasStyle->SetLabelSize(tsize,"x");
  atlasStyle->SetTitleSize(tsize,"x");
  atlasStyle->SetLabelSize(tsize,"y");
  atlasStyle->SetTitleSize(tsize,"y");
  atlasStyle->SetLabelSize(tsize,"z");
  atlasStyle->SetTitleSize(tsize,"z");
  atlasStyle->SetMarkerStyle(20);
  atlasStyle->SetMarkerSize(1.2);
  atlasStyle->SetHistLineWidth(2.);
  atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes
  atlasStyle->SetEndErrorSize(0.);
  atlasStyle->SetOptTitle(0);
  atlasStyle->SetOptStat(0);
  atlasStyle->SetOptFit(0);
  atlasStyle->SetPadTickX(1);
  atlasStyle->SetPadTickY(1);
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();
}

///
void Plotting_HWW::ATLASLabel(Double_t x,Double_t y) 
{
  TLatex l;
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(kBlack);
  l.SetTextSize(0.06);  
  double delx = 0.16;
  l.DrawLatex(x,y,"ATLAS");
  TLatex p; 
  p.SetNDC();
  p.SetTextFont(42);
  p.SetTextColor(kBlack);
  p.SetTextSize(0.06);  
  p.DrawLatex(x+delx,y,"Open Data");

  return;
}

///
void Plotting_HWW::WhichFiles(){
  // read in file
  std::string ifile = "Files.txt";
  ifstream input(ifile.c_str());
  std::string line;
  while(getline(input,line)){
    if (line.find("###") != line.npos )continue;
    std::string name, xsec, sumw, eff;
    istringstream linestream(line);
    getline(linestream, name, '|');
    getline(linestream, xsec, '|');
    getline(linestream, sumw, '|');
    getline(linestream, eff);
    if (DEBUG) cout << name << " " << xsec << " " << sumw << " " << eff << endl;
    float sumw_eff = atof(sumw.c_str()) * atof(eff.c_str());
    SF[name] = std::make_pair(atof(xsec.c_str()), sumw_eff);    

  }
 return;
}

///
void Plotting_HWW::readFiles(){

  // THIS IS THE DIRECTORY FROM WHERE HISTOS ARE TAKEN
//if option.contains("Wana")  
//std::string readname = "/afs/cern.ch/user/y/ylo/Plotting_HWW/Input_HWW_0723_2";
  //std::string readname = "/afs/cern.ch/user/y/ylo/Plotting_HWW/Input_W_0801";
  std::string readname = "/afs/cern.ch/user/y/ylo/Plotting_HWW/Input_Z_0810";
  //std::string readname = "/afs/cern.ch/user/y/ylo/Plotting_HWW/Input_Top_0730";
  //std::string readname = "/afs/cern.ch/user/y/ylo/Plotting_HWW/Input_ZPrime_0807";


  std::map<std::string,std::pair<double,double> >::const_iterator SFiter;
  for(SFiter = SF.begin(); SFiter != SF.end(); SFiter++){
    std::string readme = readname + "/" + SFiter->first + ".root";
    if (DEBUG)  std::cout << "Reading file: " << readme << std::endl;
    TFile* file  = TFile::Open(readme.c_str(), "READ");
   
    createHistoSet(file, SFiter->first);
   
    // apply Scale Factors: scaling is done by calculating the luminosity of the sample via xsec/(sumw*red_eff) and  multiplying the target luminosity
    if(std::strstr((SFiter->first).c_str(),"data") == NULL ){
      std::vector<std::string> v_n; v_n.clear();
      std::string n = SFiter->first;
      v_n.push_back(n);
      for(unsigned int i = 0; i < v_n.size(); ++i){
        std::map<std::string,TH1F*>::const_iterator fIter = histo[v_n[i]].begin();
        std::map<std::string,TH1F*>::const_iterator lIter = histo[v_n[i]].end();
        double scf = 1;
        scf = (lumi *  SFiter->second.first) / SFiter->second.second ; 
        if (DEBUG) cout << " SF is: " << scf << endl;
        for(; fIter!=lIter;++fIter){
          fIter->second->Scale(scf);
          //fIter->second->Rebin(2);
        }
      }
    }
 
  }
  return;
}

///
void Plotting_HWW::createHistoSet(TFile* file, std::string proc){
  
  std::vector<HistoHandler*>::iterator fIter = hset.begin();
  std::vector<HistoHandler*>::iterator lIter = hset.end();
 
  for(; fIter != lIter; ++fIter){
    std::string n;
    n  = (*fIter)->GetName();
    if (DEBUG) cout << "reading histogram: " << n << endl;
    createHistogram(file, n, proc);  
  }
    
  return;
}


///
void Plotting_HWW::createHistogram(TFile* file, std::string hname ,  std::string proc){

  histo[proc][hname] = (TH1F*)file->Get(hname.c_str()); 
 
  return;
}

///
void Plotting_HWW::makePlots(){

  TCanvas* c;
  c = new TCanvas("c","c",700,750);
  TPad* pad0; //upper
  TPad* pad1; //lower
  TPad* padX; //mask

  pad0 = new TPad("pad0","pad0",0,0.29,1,1,0,0,0);
  pad0->SetTickx(false);
  pad0->SetTicky(false);
  pad0->SetTopMargin(0.05);
  pad0->SetBottomMargin(0);
  pad0->SetLeftMargin(0.14);
  pad0->SetRightMargin(0.05);
  pad0->SetFrameBorderMode(0);
  pad0->SetTopMargin(0.06);

  pad1 = new TPad("pad1","pad1",0,0,1,0.29,0,0,0);
  pad1->SetTickx(false);
  pad1->SetTicky(false);
  pad1->SetTopMargin(0.0);
  pad1->SetBottomMargin(0.5);
  pad1->SetLeftMargin(0.14);
  pad1->SetRightMargin(0.05);
  pad1->SetFrameBorderMode(0);

 
  padX = new TPad("pad1","pad1",0.07, 0.27, 0.135, 0.29, 0, 0, 0);
  padX->SetTickx(false);
  padX->SetTicky(false);
  padX->SetTopMargin(0.0);
  padX->SetBottomMargin(0.5);
  padX->SetLeftMargin(0.14);
  padX->SetRightMargin(0.05);
  padX->SetFrameBorderMode(0);

  pad1->Draw();
  pad0->Draw();
  padX->Draw();

  pad0->cd();

///////////////////////////////////////////////////////////////////////

//data
  std::map<std::string,TH1F*> data; 

//Higgs
  std::map<std::string,TH1F*> ggH125_WW2lep;
  //std::map<std::string,TH1F*> VBFH125_WW2lep;

//ttbar
  std::map<std::string,TH1F*> ttbar_lep;   
  std::map<std::string,TH1F*> ttbar_had;

//Z+jets
  std::map<std::string,TH1F*> Z_ee;
  std::map<std::string,TH1F*> Z_mumu;
  std::map<std::string,TH1F*> Z_tautau;
//DY  
  std::map<std::string,TH1F*> DYeeM08to15;
  std::map<std::string,TH1F*> DYeeM15to40;
  std::map<std::string,TH1F*> DYmumuM08to15;
  std::map<std::string,TH1F*> DYmumuM15to40;
  std::map<std::string,TH1F*> DYtautauM08to15;
  std::map<std::string,TH1F*> DYtautauM15to40;
//to add: 'DYeeM08to15, 'DYeeM15to40', 'DYmumuM08to15', 'DYmumuM15to40', 'DYtautauM08to15', 'DYtautauM15to40'

// diboson
  std::map<std::string,TH1F*> WW;
  std::map<std::string,TH1F*> WZ;
  std::map<std::string,TH1F*> ZZ;

//W+jets
  std::map<std::string,TH1F*> WenuWithB;
  std::map<std::string,TH1F*> WenuJetsBVeto;
  std::map<std::string,TH1F*> WenuNoJetsBVeto;
  std::map<std::string,TH1F*> WmunuWithB;
  std::map<std::string,TH1F*> WmunuJetsBVeto;
  std::map<std::string,TH1F*> WmunuNoJetsBVeto;
  std::map<std::string,TH1F*> WtaunuWithB;
  std::map<std::string,TH1F*> WtaunuJetsBVeto;
  std::map<std::string,TH1F*> WtaunuNoJetsBVeto;
//to add 'WenuJetsBVeto', 'WenuWithB', 'WenuNoJetsBVeto', 'WmunuJetsBVeto', 'WmunuWithB', 'WmunuNoJetsBVeto', 'WtaunuJetsBVeto', 'WtaunuWithB', 'WtaunuNoJetsBVeto'

//single top
  std::map<std::string,TH1F*> stop_tchan_top;
  std::map<std::string,TH1F*> stop_tchan_antitop;
  std::map<std::string,TH1F*> stop_schan;
  std::map<std::string,TH1F*> stop_wtchan;

//ZPrime
  std::map<std::string,TH1F*> ZPrime400;
  std::map<std::string,TH1F*> ZPrime500;
  std::map<std::string,TH1F*> ZPrime750;
  std::map<std::string,TH1F*> ZPrime1000;
  std::map<std::string,TH1F*> ZPrime1250;
  std::map<std::string,TH1F*> ZPrime1500;
  std::map<std::string,TH1F*> ZPrime1750;
  std::map<std::string,TH1F*> ZPrime2000;
  std::map<std::string,TH1F*> ZPrime2250;
  std::map<std::string,TH1F*> ZPrime2500;
  std::map<std::string,TH1F*> ZPrime3000;


// to add 'stop_tchan_top', 'stop_tchan_antitop', 'stop_schan', 'stop_wtchan'




// naming of the histos, must be the same as in Files.txt, and follow previous lines
  data = histo["data"];
  ggH125_WW2lep = histo["ggH125_WW2lep"];
  //VBFH125_WW2lep = histo["VBFH125_WW2lep"];

// to add ttbar_had
  ttbar_lep = histo["ttbar_lep"];
  ttbar_had = histo["ttbar_had"];


  Z_ee = histo["Z_ee"];
  Z_mumu = histo["Z_mumu"];
  Z_tautau = histo["Z_tautau"];
  DYeeM08to15 = histo["DYeeM08to15"];
  DYeeM15to40 = histo["DYeeM15to40"];
  DYmumuM08to15 = histo["DYmumuM08to15"];
  DYmumuM15to40 = histo["DYmumuM15to40"];
  DYtautauM08to15 = histo["DYtautauM08to15"];
  DYtautauM15to40 = histo["DYtautauM15to40"];


  WW = histo["WW"]; 
  WZ = histo["WZ"];
  ZZ = histo["ZZ"];

  WenuWithB = histo["WenuWithB"];
  WenuJetsBVeto = histo["WenuJetsBVeto"];
  WenuNoJetsBVeto = histo["WenuNoJetsBVeto"];
  WmunuWithB = histo["WmunuWithB"];
  WmunuJetsBVeto = histo["WmunuJetsBVeto"];
  WmunuNoJetsBVeto = histo["WmunuNoJetsBVeto"];
  WtaunuWithB = histo["WtaunuWithB"];
  WtaunuJetsBVeto = histo["WtaunuJetsBVeto"];
  WtaunuNoJetsBVeto = histo["WtaunuNoJetsBVeto"];

  stop_tchan_top = histo["stop_tchan_top"];
  stop_tchan_antitop = histo["stop_tchan_antitop"];
  stop_schan = histo["stop_schan"];
  stop_wtchan = histo["stop_wtchan"];

  ZPrime400 = histo["ZPrime400"];
  ZPrime500 = histo["ZPrime500"];
  ZPrime750 = histo["ZPrime750"];
  ZPrime1000 = histo["ZPrime1000"];
  ZPrime1250 = histo["ZPrime1250"];
  ZPrime1500 = histo["ZPrime1500"];
  ZPrime1750 = histo["ZPrime1750"];
  ZPrime2000 = histo["ZPrime2000"];
  ZPrime2250 = histo["ZPrime2250"];
  ZPrime2500 = histo["ZPrime2500"];
  ZPrime3000 = histo["ZPrime3000"];


//...

///////////////////////////////////////////////////////////////////////
//begin plotting

  std::map<std::string,TH1F*>::const_iterator fIter;
  std::map<std::string,TH1F*>::const_iterator lIter;
  fIter = data.begin(); lIter = data.end();
  for(; fIter != lIter; ++fIter){

    std::cout<<"Plotting histogram: "<< fIter->first << std::endl;
    
    // data style
    fIter->second->SetMarkerStyle(20);
    fIter->second->SetMarkerColor(kBlack);
    fIter->second->SetMarkerSize(1.2);
    fIter->second->SetLineWidth(2);
    fIter->second->SetMinimum(0.1);
    fIter->second->Draw("E1");
    gStyle->SetEndErrorSize(1.); 
    TGaxis::SetMaxDigits(4);
    fIter->second->GetYaxis()->SetTitleOffset(1.2);
    //fIter->second->Rebin(2);

//////////////////// begin merging of different MC



// merge Higgs
    //TH1F* Higgs = (TH1F*)ggH125_WW2lep[fIter->first]->Clone();
    //Higgs->Add(VBFH125_WW2lep[fIter->first]);
    //Higgs->SetFillColor(kRed);
    //Higgs->SetLineWidth(0);

// merge ZPrime
    ////TH1F* ZPrime = (TH1F*)ZPrime400[fIter->first]->Clone();
    //TH1F* ZPrime = (TH1F*)ZPrime1000[fIter->first]->Clone();
    //ZPrime->SetFillColor(kRed);
    //ZPrime->SetLineWidth(0);


// merge Z MCs
    //TH1F* Z = (TH1F*)Z_ee[fIter->first]->Clone();
    //Z->Add(Z_mumu[fIter->first]);
    //Z->Add(Z_tautau[fIter->first]);
//to add 'DYeeM08to15, 'DYeeM15to40', 'DYmumuM08to15', 'DYmumuM15to40', 'DYtautauM08to15', 'DYtautauM15to40'
    //Z->SetFillColor(kYellow);
    //Z->SetLineWidth(0);

// merge DY
    //TH1F* DY = (TH1F*)DYeeM08to15[fIter->first]->Clone();
    //DY->Add(DYeeM15to40[fIter->first]);
    //DY->Add(DYmumuM08to15[fIter->first]);
    //DY->Add(DYmumuM15to40[fIter->first]);
    //DY->Add(DYtautauM08to15[fIter->first]);
    //DY->Add(DYtautauM15to40[fIter->first]);
    //DY->SetFillColor(kYellow+2);
    //DY->SetLineWidth(0);
   
// merge diboson
    TH1F* diboson = (TH1F*)WW[fIter->first]->Clone();
    diboson->Add(WZ[fIter->first]);
    diboson->Add(ZZ[fIter->first]);
    //diboson->SetFillColor(kOrange-3);
    diboson->SetFillColor(kBlue-6);
    diboson->SetLineWidth(0);


// merge ttbar
    TH1F* ttbar = (TH1F*)ttbar_lep[fIter->first]->Clone();
    ttbar->Add(ttbar_had[fIter->first]);
    ttbar->SetFillColor(kOrange-3);
    ttbar->SetLineWidth(0);

// merge W+jets
    TH1F* W = (TH1F*)WenuWithB[fIter->first]->Clone();
    W->Add(WenuJetsBVeto[fIter->first]);
    W->Add(WenuNoJetsBVeto[fIter->first]);
    W->Add(WmunuWithB[fIter->first]);
    W->Add(WmunuJetsBVeto[fIter->first]);
    W->Add(WmunuNoJetsBVeto[fIter->first]);
    W->Add(WtaunuWithB[fIter->first]);
    W->Add(WtaunuJetsBVeto[fIter->first]);
    W->Add(WtaunuNoJetsBVeto[fIter->first]);
    //V->Add(DYeeM08to15[fIter->first]);
    //V->Add(DYeeM15to40[fIter->first]);
    //V->Add(DYmumuM08to15[fIter->first]);
    //V->Add(DYmumuM15to40[fIter->first]);
    //V->Add(DYtautauM08to15[fIter->first]);
    //V->Add(DYtautauM15to40[fIter->first]);
    //V->Add(Z_mumu[fIter->first]);
    //V->Add(Z_tautau[fIter->first]);
    //V->Add(Z_ee[fIter->first]);
    W->SetFillColor(kGreen-3);
    W->SetLineWidth(0);
//merge Z+jets
    TH1F* Z = (TH1F*)Z_mumu[fIter->first]->Clone();
    Z->Add(Z_tautau[fIter->first]);
    Z->Add(Z_ee[fIter->first]);
    Z->Add(DYeeM08to15[fIter->first]);
    Z->Add(DYeeM15to40[fIter->first]);
    Z->Add(DYmumuM08to15[fIter->first]);
    Z->Add(DYmumuM15to40[fIter->first]);
    Z->Add(DYtautauM08to15[fIter->first]);
    Z->Add(DYtautauM15to40[fIter->first]);
    Z->SetFillColor(kPink+9);
    Z->SetLineWidth(0);
 
//merge single top
    //TH1F* stop = (TH1F*)stop_tchan_top[fIter->first]->Clone();
    TH1F* stop = (TH1F*)stop_wtchan[fIter->first]->Clone();
    //stop->Add(stop_tchan_antitop[fIter->first]);
    stop->Add(stop_schan[fIter->first]);
    //stop->Add(stop_wtchan[fIter->first]);
    stop->SetFillColor(kAzure+8);
    stop->SetLineWidth(0);
    //cout<<stop->Integral()<<endl;


////////////////////////////////////////////////////////////////////////////////////////////////

// main stack of MCs
    THStack* stack = new THStack();
    //stack->Add(ttbar);
    //stack->Add(V);
    //stack->Add(diboson);
    //stack->Add(WW);
    //stack->Add(DY);
    stack->Add(stop);
    stack->Add(ttbar);
    //stack->Add(DY);    
    stack->Add(diboson);
    stack->Add(W);
    stack->Add(Z);
    //stack->Add(stop);
    //stack->Add(W);
    //stack->Add(V);
    //stack->Add(ttbar);
    //stack->Add(ZPrime);
    //stack->Add(W);
    //stack->Add(Higgs);




// statistical error histogram, add all MCs to it
    //TH1F* histstack = (TH1F*)diboson->Clone();
    //histstack->Add(ttbar);
    //histstack->Add(V);
    //histstack->Add(W);
    //histstack->Add(DY);
    //histstack->Add(stop);

    TH1F* histstack = (TH1F*)stop->Clone();
    histstack->Add(ttbar);
    //histstack->Add(DY);
    histstack->Add(diboson);
    //histstack->Add(V);
    histstack->Add(Z);
    histstack->Add(W);
    //histstack->Add(ZPrime);

    histstack->SetFillStyle(3454);
    histstack->SetFillColor(kBlue+2);
    histstack->SetLineColor(kBlack);
    histstack->SetLineWidth(2);
    histstack->SetMarkerSize(0);
    histstack->SetLineColor(kWhite);

// obtain the statistical uncertainty
    float err;
    int nbin = histstack->GetNbinsX();
    for(int i_bin=0;i_bin<=nbin;i_bin++){
      err = histstack->GetBinError(i_bin);
      histstack->SetBinError(i_bin, err );
    }

    cout<<ggH125_WW2lep[fIter->first]->Integral()<<endl;
// add normalized Higgs signal
    TH1F* h_normsig = (TH1F*)ggH125_WW2lep[fIter->first]->Clone();
    //h_normsig->Add(VBFH125_WW2lep[fIter->first]);
    //h_normsig->Scale(histstack->Integral()/h_normsig->Integral());
    //h_normsig->SetLineColor(kRed);
    //h_normsig->SetFillStyle(0);
    //h_normsig->SetLineStyle(2);
    //h_normsig->SetFillColor(2);
    //h_normsig->SetLineWidth(2);
    //
    
 //ZPrime NF
    //TH1F* ZP_normsig = (TH1F*)ZPrime1000[fIter->first]->Clone(); 
    //ZP_normsig->Scale(histstack->Integral()/ZP_normsig->Integral());
    //ZP_normsig->SetLineColor(kRed);
    //ZP_normsig->SetFillStyle(0);
    //ZP_normsig->SetLineStyle(2);
    //ZP_normsig->SetFillColor(2);
    //ZP_normsig->SetLineWidth(2);

// set Yaxis maximum
    float yMaxScale = 2.;
    fIter->second->SetMaximum(yMaxScale*TMath::Max( TMath::Max(fIter->second->GetMaximum(),histstack->GetMaximum()), h_normsig->GetMaximum() ) );


    // latex options 
    TLatex l;
    l.SetNDC();
    l.SetTextColor(kBlack);
    l.SetTextFont(42);
    l.SetTextSize(0.04);
    //l.DrawLatex(0.18,0.71,"H#rightarrowWW#rightarrowl#nul#nu + 0 jet ");
    //l.DrawLatex(0.18,0.71,"Z'#rightarrowl#nubqqb");
    l.DrawLatex(0.18,0.71,"Z#rightarrowll");
    //l.DrawLatex(0.18,0.71,"W#rightarrowlv");

    TLatex l2;
    l2.SetNDC();
    l2.SetTextSize(0.04);  
    l2.SetTextColor(kBlack);
    l2.DrawLatex(0.18,0.79, Form("#sqrt{s} = 8 TeV, #int L dt = 1 fb^{-1}")); 


//create legend
    TLegend* leg;
    leg  = new TLegend();
    leg  = new TLegend(0.70,0.60,0.93,0.925);  
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);
    leg->SetTextAlign(32);
    leg->SetTextFont(42);
    leg->SetTextSize(0.05);
    leg->SetMargin(0.22);
    leg->SetTextAlign(32);

// fill HWW legend
    //leg-> AddEntry(data[fIter->first] , "Data" ,"lep");
    //leg-> AddEntry(Higgs , "Higgs", "f");
    //leg-> AddEntry(diboson , "Diboson", "f");
    //leg-> AddEntry(V,  "V+jets", "f");
    //leg-> AddEntry(ttbar, "t#bar{t}", "f");
    //leg-> AddEntry(stop, "Single top", "f");
    //leg-> AddEntry(histstack,"Stat. unc.","f");
    //leg-> AddEntry(h_normsig, "Higgs_{norm.}" ,"l");

// fill W legend
    leg-> AddEntry(data[fIter->first] , "Data" ,"lep");
    leg-> AddEntry(Z , "Z+jets", "f");
    leg-> AddEntry(W , "W+jets", "f");
    //leg-> AddEntry(Z , "Z+jets", "f");
    leg-> AddEntry(diboson , "Diboson", "f");
    //leg-> AddEntry(DY,  "DY", "f");
    leg-> AddEntry(ttbar, "t#bar{t}", "f");
    leg-> AddEntry(stop, "Single top", "f");
    leg-> AddEntry(histstack,"Stat. unc.","f");

// fill ZPrime legend
    //leg-> AddEntry(data[fIter->first] , "Data" ,"lep");
    //leg-> AddEntry(ZPrime , "Z'(1TeV)", "f");
    //leg-> AddEntry(ttbar, "t#bar{t}", "f");
    //leg-> AddEntry(V , "V+jets", "f");
    ////leg-> AddEntry(W , "W+jets", "f");
    //leg-> AddEntry(stop, "Single top", "f");
    ////leg-> AddEntry(Z , "Z+jets", "f");
    //leg-> AddEntry(diboson , "Diboson", "f");
    ////leg-> AddEntry(DY,  "DY", "f");
    //leg-> AddEntry(histstack,"Stat. unc.","f");
    //leg-> AddEntry(ZP_normsig, "Z'_{norm.}" ,"l");
    
    // draw everything 
    stack->Draw("HISTsame");
    histstack->Draw("e2same");
    fIter->second->Draw("sameE1");
    h_normsig->Draw("HISTsame");
    //ZP_normsig->Draw("HISTsame");
    leg->Draw("same");
    
    ATLASLabel(0.18, 0.87);


    ////////////////////////////////////////////////////////////////////////////////
    // lower pad contains the ratio of data to MC
    pad1->cd();
    pad1->GetFrame()->SetY1(2);
    pad1->Draw();

    // create the ratio plot  
    TH1F *h_ratio = (TH1F*)data[fIter->first]->Clone("h_ratio");
    h_ratio->Divide((TH1F*)histstack->Clone());
    h_ratio->GetYaxis()->SetTitle("Data / Pred      ");

    // add a line in 1
    TLine *hline;
    hline = new TLine(h_ratio->GetXaxis()->GetXmin(),1,h_ratio->GetXaxis()->GetXmax(),1);
    hline->SetLineColor(kGray+2);
    hline->SetLineWidth(2);
    hline->SetLineStyle(1);
   

    gStyle->SetEndErrorSize(1.);
    h_ratio->GetYaxis()->CenterTitle();
    h_ratio->GetYaxis()->SetNdivisions(504,false);
    h_ratio->Draw("0E1");
    hline->Draw();

    // cosmetics
    h_ratio->SetMinimum(0);
    h_ratio->SetMaximum(2);
    h_ratio->GetXaxis()->SetTitle(  fIter->second->GetXaxis()->GetTitle()  );
    h_ratio->GetXaxis()->SetTitleSize(0.15);
    h_ratio->GetXaxis()->SetLabelSize(0.13);
    h_ratio->GetXaxis()->SetTitleOffset(1.2);
    h_ratio->GetYaxis()->SetTitleSize(0.12);
    h_ratio->GetYaxis()->SetTitleOffset(0.5);
    h_ratio->GetYaxis()->SetLabelSize(0.10);
 
    data[fIter->first]->GetXaxis()->SetLabelSize(0);
    gPad->RedrawAxis();
    pad1->cd();
    pad1->cd()->SetGridy();
  h_ratio->Draw("SAME0E1");
  h_ratio->Draw("SAMEAXIS");
  h_ratio->GetYaxis()->Draw();
  h_ratio->Draw("SAME0E1");

  pad0->cd();
  TAxis * Ay1 = NULL;
  Ay1 = fIter->second->GetXaxis();
  Ay1->SetLabelSize(0);
  Ay1->SetTitleSize(0);
  gPad->RedrawAxis();

  // printing the canvas to a png (pdf...) file    
  PrintCanvas(c,fIter->first);

 }

  return;
}

///
void Plotting_HWW::PrintCanvas(TCanvas* c1, string title){

  string outFolder="histograms";
  std::string tpng = outFolder+"/"+title+".png";
  c1->SaveAs(tpng.c_str());

// uncomment to get plot saved in pdf
// std::string tpdf = outFolder+"/"+title+".pdf";
// c1->SaveAs(tpdf.c_str());

  return;
}

///
void Plotting_HWW::getHistoSettings(){

  // save names of the histograms for later  
  hset.clear();

  // read in configuration file

  std::string ifile = "HistoList_Z.txt";
  ifstream input(ifile.c_str());
  std::string line;
  while(getline(input,line)){
    if (line.find("#") != line.npos )continue;
    std::string n;
    istringstream linestream(line);
    getline(linestream, n, '\n');
    HistoHandler* h = new HistoHandler(n);
    hset.push_back(h); 
  }
  return;
}

///
HistoHandler::HistoHandler(){
}

///
HistoHandler:: HistoHandler(std::string name){
  _name = name;
}

///
HistoHandler::~HistoHandler(){
}

///
std::string HistoHandler::GetName(){
  return _name;
}
///////////////////////////////////////////////////////
